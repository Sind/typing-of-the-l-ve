function love.conf(t)
	t.identity = "TypingOfTheLove"
	t.version = "0.9.0"

	t.modules.joystick = false
	t.modules.physics = false
end