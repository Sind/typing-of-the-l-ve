menu = {}

diffcolor = {Easy = {0,255,0},Normal = {255,255,0},Hard = {255,153,00},Jonathan = {255,0,0}}

function menu.init()
	menuMode = 1
	option = 1
	numoptions = 1
	alphaTimer = 0
	alpham = 0
	exitm = false
	if endaudio then love.audio.stop(); endaudio = false end
	love.audio.play(music["menu"])
end

function menu.update(dt)
	if exitm then
		alpham = alpham + dt * 1000
		if alpham >255 then exit = true end
		return
	end
	if keypressed["down"] then
		if menuTree[menuMode].options[option].down then menuTree[menuMode].options[option].down()
		else
			option = option + 1
			if option > #menuTree[menuMode].options then option = option - #menuTree[menuMode].options end
		end
	end

	if keypressed["up"] then
		if menuTree[menuMode].options[option].up then menuTree[menuMode].options[option].up()
		else
			option = option - 1
			if option < 1 then option = option + #menuTree[menuMode].options end
		end
	end

	if keypressed["right"] then
		if menuTree[menuMode].options[option].inc then menuTree[menuMode].options[option].inc(1) end
	end

	if keypressed["left"] then
		if menuTree[menuMode].options[option].inc then menuTree[menuMode].options[option].inc(-1) end
	end

	if keypressed["return"] and menuTree[menuMode].options[option].run then menuTree[menuMode].options[option].run() end
end
function menu.exit()
	exitm = false
	mode = modeset
end
function menu.draw()

	love.graphics.draw(mainBackground)
	dispenser.animation:draw(dispenser.image,SPAWN_X,SPAWN_Y,0,1,1,21,36)
	conveyor.animation:draw(conveyor.image,0,CONVEYOR_HEIGHT-10)
	love.graphics.setBlendMode("additive")
	love.graphics.draw(light)
	love.graphics.setBlendMode("alpha")
	player.draw(320/2,170,0,-1,1,50,50)

	if menuTree[menuMode].draw then menuTree[menuMode].draw() end
	for i,v in ipairs(menuTree[menuMode].options) do
		if v.draw then if i == option then v.draw(true) else v.draw() end end
	end

end

menuTree = {
	[1] = {
		draw = function(current)
			if exitm then
				love.graphics.setColor(0,0,0,alpham)
				love.graphics.rectangle("fill",0,0,320,400)
				love.graphics.setColor(255,255,255)
			end
		end,
		options = {
			[1] = {
				run = function()
					exitm = true
					modeset = "play"
				end,
				draw = function(current)
					love.graphics.rectangle("fill", 110,15-7,100,12)
					love.graphics.setColor(128,50,50)
					love.graphics.rectangle("fill", 111, 16-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("Start Game",112,17-7)
					love.graphics.setColor(255,255,255)
				end
			},
			[2] = {
				run = function()
					exitm = true
					modeset = "create"
				end,
				draw = function(current)
					love.graphics.rectangle("fill", 110,30-7,100,12)
					love.graphics.setColor(128,50,50)
					love.graphics.rectangle("fill", 111, 31-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("Change Character",112,32-7)
					love.graphics.setColor(255,255,255)
				end
			},
			[3] = {
				run = function()
					menuMode = 3
					option = 1
				end,
				draw = function(current)
					love.graphics.rectangle("fill", 110,46-7,100,12)
					love.graphics.setColor(128,50,40)
					love.graphics.rectangle("fill", 111, 47-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("Settings",112,48-7)
					love.graphics.setColor(255,255,255)
				end
			},
			[4] = {
				run = function()
					menuMode = 4
					option = 1
				end,
				draw = function(current)
					love.graphics.rectangle("fill", 110,62-7,100,12)
					love.graphics.setColor(128,50,50)
					love.graphics.rectangle("fill", 111, 63-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("Credits",112,64-7)
					love.graphics.setColor(255,255,255)
				end
			},
			[5] = {
				run = function()
					love.filesystem.write("settings.lua","return {scale = " .. settings.scale .. ", fullscreen = " .. (settings.fullscreen and "true" or "false") ..", volume = ".. settings.volume .. ", difficulty = \"" .. settings.difficulty .. "\"}")
					love.event.push("quit")
				end,
				draw = function(current)
					love.graphics.rectangle("fill", 110,78-7,100,12)
					love.graphics.setColor(128,50,50)
					love.graphics.rectangle("fill", 111, 79-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("Exit",112,80-7)
					love.graphics.setColor(255,255,255)
				end
			}
		}
	},
	[2] = {
		draw = function(current)
		end,
		options = {
			[1] = {
				run = function()
					menuMode = 1
					option = 2
				end
			}
		}
	},
	[3] = {
		draw = function(current) end,
		options = {
			[1] = { -- screen
				draw = function(current)
					love.graphics.rectangle("fill", 110,15-7,100,12)
					love.graphics.setColor(128,50,50)
					love.graphics.rectangle("fill", 111, 16-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("Scale:" .. settings.scale,112,17-7)
					love.graphics.setColor(255,255,255)
				end,
				inc = function(dir)
					settings.scale = settings.scale + dir
					if settings.scale > 9 then settings.scale = 1 end
					if settings.scale < 1 then settings.scale = 9 end
					updateWindow()
				end
			},
			[2] = { -- fullscreen
				draw = function (current)
					love.graphics.rectangle("fill", 110,30-7,100,12)
					love.graphics.setColor(128,50,50)
					love.graphics.rectangle("fill", 111, 31-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("Fullscreen:" .. (settings.fullscreen and "ON" or "OFF"),112,32-7)
					love.graphics.setColor(255,255,255)
				end,
				run = function()
					settings.fullscreen = not settings.fullscreen
					updateWindow()
				end,
				inc = function()
					settings.fullscreen = not settings.fullscreen
					updateWindow()
				end
			},
			[3] = { -- volume
				draw = function(current)
					love.graphics.rectangle("fill", 110,46-7,100,12)
					love.graphics.setColor(128,50,40)
					love.graphics.rectangle("fill", 111, 47-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("Volume:" .. settings.volume,112,48-7)
					love.graphics.setColor(255,255,255)
				end,
				inc = function(dir)
					settings.volume = settings.volume + 10 * dir
					if settings.volume > 100 then settings.volume = 100
					elseif settings.volume < 0 then settings.volume = 0
					else love.audio.setVolume(settings.volume/100) end
					playSound("correct")
				end
			},
			[4] = {
				draw = function(current)

					love.graphics.rectangle("fill", 110,62-7,100,12)
					love.graphics.setColor(128,50,50)
					love.graphics.rectangle("fill", 111, 63-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("diff:",112,64-7)
					love.graphics.setColor(diffcolor[settings.difficulty])
					love.graphics.print(settings.difficulty,141,64-7)
					love.graphics.setColor(255,255,255)
				end,
				inc = function(dir)
					if dir == 1 then
						if settings.difficulty == "Easy" then
							settings.difficulty = "Normal"
						elseif settings.difficulty == "Normal" then
							settings.difficulty = "Hard"
						elseif settings.difficulty == "Hard" then
							settings.difficulty = "Jonathan"
						else
							settings.difficulty = "Easy"
						end
					else
						if settings.difficulty == "Easy" then
							settings.difficulty = "Jonathan"
						elseif settings.difficulty == "Normal" then
							settings.difficulty = "Easy"
						elseif settings.difficulty == "Hard" then
							settings.difficulty = "Normal"
						else
							settings.difficulty = "Hard"
						end
					end
				end
			},
			[5] = {
				draw = function(current)
					love.graphics.rectangle("fill", 110,78-7,100,12)
					love.graphics.setColor(128,50,50)
					love.graphics.rectangle("fill", 111, 79-7, 98, 10)
					love.graphics.setColor(current and {0,255,0} or {255,255,255})
					love.graphics.print("Back",112,80-7)
					love.graphics.setColor(255,255,255)
				end,
				run = function()
					menuMode = 1
					option = 3
				end
			}
		}
	},
	[4] = {
		draw = function(current)

		love.graphics.rectangle("fill", 44, 18,240,60)
		love.graphics.setColor(128,50,50)
		love.graphics.rectangle("fill", 45, 19, 238, 58)
		love.graphics.setColor(255,255,255)
			love.graphics.print([[
Srod Karim        - Programming

Jonathan Ringstad - Art and aniamtions

Julian Nymark     - Additional work

Preben Aas        - Music and Sounds]],46,20)
		end,
		options = {
			[1] = {
				run = function()
					menuMode = 1
					option = 4
				end
			}
		}
	}
}