typeword = class()

function typeword:init(w)
	self.word = w
	self.totype = w
	self.typed = ""
	self.wrong = ""
	self.mistakes = 0
end

function typeword:typedL(l)
	if l == self.totype:sub(1,1) then
		-- playSound("type")
		self.typed = self.typed .. l
		if self.totype:len() == 1 then
			return "happy"
		else
			self.totype = self.totype:sub(2,self.totype:len())
		end
	else
		self.wrong = self.word:sub(1,self.typed:len()+1)
		if self.mistakes == 2 then
			return "angry"
		else
			self.mistakes = self.mistakes + 1
		end
	end
	return false
end


function typeword:failed()

end

function typeword:comlpeted()

end

function typeword:len()
	return self.word:len()
end
function typeword:sub(...)
	return self.word:sub(...)
end

function typeword:print(...)
	love.graphics.print(self.word,...)
	love.graphics.setColor(255,0,0)
	love.graphics.print(self.wrong,...)
	love.graphics.setColor(0,255,0)
	love.graphics.print(self.typed,...)
	love.graphics.setColor(255,255,255)
end
