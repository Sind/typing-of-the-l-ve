create = {}
-- make the caracter creator here

-- this runs at the start of the mode
-- load all the assets you need here
function create.init()
		background = love.graphics.newImage("graphics/bg_creation.png")
	
	selection_category = 1

	select_hair = {}
	select_hair.select = 1
	-- table.insert(select_hair, 2) -- current selection in category
	for i,v in ipairs(love.filesystem.getDirectoryItems("graphics/player_hair")) do
		table.insert(select_hair, love.graphics.newImage("graphics/player_hair/"..v))
	end

	select_chair = {}
	select_chair.select = 1
	-- table.insert(select_chair, 2) -- current selection in category
	for i,v in ipairs(love.filesystem.getDirectoryItems("graphics/player_chair")) do
		table.insert(select_chair, love.graphics.newImage("graphics/player_chair/"..v))
	end

	select_mustache = {}
	select_mustache.select = 1
	-- table.insert(select_mustache, 2) -- current selection in category
	for i,v in ipairs(love.filesystem.getDirectoryItems("graphics/player_beard")) do
		table.insert(select_mustache, love.graphics.newImage("graphics/player_beard/"..v))
	end

	select_eyes = {}
	select_eyes.select = 1
	-- table.insert(select_eyes, 2) -- current selection in category
	for i,v in ipairs(love.filesystem.getDirectoryItems("graphics/player_eyes")) do
		table.insert(select_eyes, love.graphics.newImage("graphics/player_eyes/"..v))
	end

	select_category = {}
	table.insert(select_category, select_hair) -- 1
	table.insert(select_category, select_chair) -- 2
	table.insert(select_category, select_eyes)
	table.insert(select_category, select_mustache)

	currColor = {1,1}
end

-- runs constantly.
-- check for keypresses using keypressed[]
-- so, "keypressed["down"]" is true if the down key is pressed
-- don't use the mouse
function create.update(dt)
	local random = love.math.random

	if keypressed["return"] or keypressed[" "] then
		exit = true
		modeset = "menu"
	end

	if keypressed["up"] then
		if selection_category == 1 then
		 currColor[1] = currColor[1] + 1
		 if currColor[1] > #hues["playerHairColor"] then
			currColor[1] = 1
		 end
		elseif selection_category == 2 then
		 currColor[2] = currColor[2] + 1
		 if currColor[2] > #hues["playerChairColor"] then
			currColor[2] = 1
		 end
		end
	end
	
	if keypressed["down"] then
		select_category[selection_category].select = select_category[selection_category].select +1
		if select_category[selection_category].select >#select_category[selection_category] then
			select_category[selection_category].select = 1
		end
	end

	if keypressed["right"] then
		selection_category = (selection_category + 1)
		if selection_category > #select_category then
		 selection_category = 1
		end
	end
	
	if keypressed["left"] then
		selection_category = (selection_category - 1)
		if selection_category < 1 then
		 selection_category = #select_category
		end
	end
	
	player.hair = select_hair[select_hair.select]
--	player.chair = select_chair[select_chair.select+2]
	player.hairColor = convertHex(hues["playerHairColor"][currColor[1]]:sub(2,7))
	player.chairColor = convertHex(hues["playerChairColor"][currColor[2]]:sub(2,7))

end

-- draw your shit here
function create.draw()
	-- bg
	love.graphics.setColor(255,255,255)
	love.graphics.draw(background)

	-- selection menu
	love.graphics.setColor(255,255,255)
	love.graphics.rectangle("fill", 48+10, 28, 6*4+3, 12)
	love.graphics.rectangle("fill", 92+10, 28, 6*5+3, 12)
	love.graphics.rectangle("fill", 148+10, 28, 6*4+3, 12)
	love.graphics.rectangle("fill", 200+10, 28, 6*8+3, 12)

	love.graphics.setColor(128, 50, 50)
	love.graphics.rectangle("fill", 49+10, 29, 6*4+1, 10)
	love.graphics.rectangle("fill", 93+10, 29, 6*5+1, 10)
	love.graphics.rectangle("fill", 149+10, 29, 6*4+1, 10)
	love.graphics.rectangle("fill", 201+10, 29, 6*8+1, 10)

	love.graphics.setColor(255,255,255)
	love.graphics.print("Hair", 50+10, 30)
	love.graphics.print("Chair", 94+10, 30)
	love.graphics.print("Eyes", 150+10, 30)
	love.graphics.print("Mustache", 202+10, 30)

	if selection_category == 1 then
		love.graphics.setColor(0, 255, 00)
		love.graphics.printf("Hair", 50+10, 30, 0, "left")
	end
	if selection_category == 2 then
		love.graphics.setColor(0, 255, 00)
		love.graphics.printf("Chair", 94+10, 30, 0, "left")
	end
	if selection_category == 3 then
		love.graphics.setColor(0, 255, 00)
		love.graphics.printf("Eyes", 150+10, 30, 0, "left")
	end
	if selection_category == 4 then
		love.graphics.setColor(0, 255, 00)
		love.graphics.printf("Mustache", 202+10, 30, 0, "left")
	end

	-- mirror display current selection in category
	if selection_category ~= 2 then 
		love.graphics.setColor(255,255,255)
		love.graphics.draw(select_category[selection_category][select_category[selection_category].select], 125, 50)

	end	

	love.graphics.setColor(255,255,255)

	-- player
	player.draw(63, 90)
end

function create.exit()
	exit = false
	mode = modeset
end
