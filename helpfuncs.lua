function updateWindow()
	love.window.setMode(settings.scale*320, settings.scale*180, {fullscreen = settings.fullscreen,fsaa = 0})
end

function convertHex(hex)
	local splitToRGB = {}

	if # hex < 6 then hex = hex .. string.rep("F", 6 - # hex) end --flesh out bad hexes

	for x = 1, # hex - 1, 2 do
		table.insert(splitToRGB, tonumber(hex:sub(x, x + 1), 16)) --convert hexes to dec
		if splitToRGB[# splitToRGB] < 0 then slpitToRGB[# splitToRGB] = 0 end --prevents negative values
	end
	return splitToRGB
end

function contains(table,element)
	for i,v in ipairs(table) do
		if v == element then return true end
	end
	return false
end