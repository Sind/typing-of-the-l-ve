
SPAWN_X, SPAWN_Y = 45,30
INIT_FALL_SPEED = 200
FALL_GRAVITY = 500
CONVEYOR_HEIGHT = 100

--fps for animations
FALLING_ANIMATION_SPEED = 5
IDLE_ANIMATION_SPEED = 5
HAPPY_ANIMATION_SPEED = 5
ANGRY_ANIMATION_SPEED = 5

DISPENSER_ANIMATION_SPEED = 20

TEXTBOX_SPEED = 200


GIRL_W, GIRL_H = 48,48

ROTATION_SPEED = math.pi

function love.load()
	love.window.setIcon(love.graphics.newImage("graphics/icon.png"):getData())
	math.randomseed( os.time() )
	love.mouse.setVisible(false)
	love.graphics.setDefaultFilter("nearest", "nearest")
	font = love.graphics.newImageFont("graphics/font.png","ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 -.,!:")
	love.graphics.setFont(font,8)
	love.window.setMode(320, 180)
	maincan = love.graphics.newCanvas()
	require "helpfuncs"
	vec2 = require "vector"
	require "class"
	require "typeword"
	require "menu"
	require "play"
	require "create"
	require "girl"
	require "typebox"
	require "player"
	require "endscreen"
	require "audio"
	require "intro"
	
	hues = require "hues"
	settings = love.filesystem.load("settings.lua")()

	player.init()

	anim8 = require "anim8"

	mainBackground = love.graphics.newImage("graphics/bg_plain.png")
	local i = love.graphics.newImage("graphics/conveyor.png")
	local g = anim8.newGrid(320, 33, i:getWidth(), i:getHeight())
	conveyor = {
		image = i,
		animation = anim8.newAnimation(g(1,'1-6'),1/6*13)
	}

	i = love.graphics.newImage("graphics/dispenser.png")
	g = anim8.newGrid(42,36, i:getWidth(), i:getHeight())
	dispenser = {
		image = i,
		animation = anim8.newAnimation(g('1-8',1),1/DISPENSER_ANIMATION_SPEED,'pauseAtEnd')
	}
	light = love.graphics.newImage("graphics/light.png")

	keys = {}
	keypressed = {}

	game = {intro = intro, menu = menu, play = play, create = create, option = option, endscreen = endscreen}
	mode = "intro"
	updateWindow()
	init = true

end

function love.update(dt)
	if init then init = false; if game[mode].init then game[mode].init() end end
	if game[mode].update then game[mode].update(dt) end
	if exit then exit = false; init = true; if game[mode].exit then game[mode].exit() end end

	keys = {}
	keypressed = {}
end

function love.draw()
	maincan:clear()
	love.graphics.setCanvas(maincan)
	if init or exit then return end
	if game[mode].draw then game[mode].draw() end
	love.graphics.setCanvas()
	love.graphics.draw(maincan,0,0,0,settings.scale)
end

function love.keypressed(key,unicode)
	if key == "escape" then love.event.quit() end
	table.insert(keys,key)
	keypressed[key] = true
end
