return {
   --list of hues	
   ["skinColor"] = {
      "#ffffff", "#ffddee", "#eeddaa", "#ffb680", "#b1642b", "#d79900"
   },
   ["topColor"] = {
      "#dd0000", "#ff6622", "#ffda21", "#33dd00", "#1133cc", "#220066", "#330044"
   },
   ["pantsColor"] = {
      "#aa0000", "#ff6622", "#ffda21", "#33dd00", "#1133cc", "#220066", "#330044"
   },
   ["eyesColor"] = {
      "#559bff", "#33cf2b"
   },
   ["mouthColor"] = {
      "#d00e5c", "#cf77bd"
   },
   ["hairColor"] = {
      "#ec6400", "#dcda08", "#634e0d", "#272643", "#00ff00", "#ff0000", "#0000ff"
   },
   ["playerHairColor"] = {
      "#ec6400", "#dcda08", "#634e0d", "#272643", "#00ff00", "#ff0000", "#0000ff"
   },
   ["playerChairColor"] = {
      "#ec6400", "#dcda08", "#634e0d", "#272643", "#00ff00", "#ff0000", "#0000ff"
   },
}
