player = {}

function player.init()
   -- things
   player.chair = love.graphics.newImage("graphics/player_chair/couch.png")
   player.head = love.graphics.newImage("graphics/player_head.png")
   player.arm = love.graphics.newImage("graphics/player_arm.png")
   player.hair = love.graphics.newImage("graphics/player_hair/shabby.png")

   -- colors
   player.chairColor = {255, 162, 122}
   player.skinColor = {245, 216, 186}
   player.hairColor = {255, 255, 50}

   -- values
   player.girlsHappy = 33 -- change this
   player.girlsTotal = 100 -- change this
end

function player.draw(...)
	--set couchcolor
   love.graphics.setColor(player.chairColor)
	--draw couch
   love.graphics.draw(player.chair, ...)
	--set player skin color
   love.graphics.setColor(player.skinColor)
	--draw player base
   love.graphics.draw(player.head, ...)
   love.graphics.draw(player.arm, ...)
   --set player hair color
   love.graphics.setColor(player.hairColor)
   love.graphics.draw(player.hair, ...)
   --draw player hair
   love.graphics.setColor(255,255,255)
end
