endscreen = {}

function endscreen.init()
   happyMeter = love.graphics.newImage("graphics/happymeter.png")
   love.audio.stop()
   love.audio.play(music["end"])
   if not wordsCorrect then
	  wordsCorrect = 100
   end
   
   if not totalwords then
	  totalwords = 100
   end
   
   endscreen.messages = {"really badly!","meh...","ok","somewhat decent","quite well","somewhat better than average",
						 "very well", "everything perfectly!"}
   
   endscreen.display_msg = 1
      
   endscreen.ratio = wordsCorrect / totalwords
end


function endscreen.update(dt)
   if endscreen.ratio < 0.4 then
	  endscreen.display_msg = 1
   elseif endscreen.ratio < 0.5 then
	  endscreen.display_msg = 2
   elseif endscreen.ratio < 0.6 then
	  endscreen.display_msg = 3
   elseif endscreen.ratio < 0.7 then
	  endscreen.display_msg = 4
   elseif endscreen.ratio < 0.8 then
	  endscreen.display_msg = 5
   elseif endscreen.ratio < 0.9 then
	  endscreen.display_msg = 6
   elseif endscreen.ratio < 1 then
	  endscreen.display_msg = 7
   else
	  endscreen.display_msg = 8
   end

   if keypressed["return"] then exit = true end
end

function endscreen.exit()
   mode = "menu"
   endaudio = true
end

function endscreen.draw()
   love.graphics.setColor(255,255,255)
   love.graphics.draw(happyMeter, 250, 10)
   
   love.graphics.setColor(0,255,0)
   love.graphics.rectangle("fill", 273, 45 + (92 * (1 - endscreen.ratio)), 
						   5, 92 * endscreen.ratio)

   love.graphics.setColor(255,255,255)
   love.graphics.printf("Your score is "..wordsCorrect, 100, 50 , 150)
   
   love.graphics.printf("You did "..endscreen.messages[endscreen.display_msg], 100, 100 , 100)

   love.graphics.setColor(255,255,255)
end
