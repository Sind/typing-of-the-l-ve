music = {}
sound = {}

music["play"] = love.audio.newSource("sounds/play_music.mp3")
music["play"]:setLooping(false)
music["play"]:setVolume(0.7)

music["menu"] = love.audio.newSource("sounds/menu_music.mp3")
music["menu"]:setLooping(true)
music["menu"]:setVolume(0.7)

music["end"] = love.audio.newSource("sounds/endscreen.wav")
music["end"]:setLooping(true)
music["end"]:setVolume(0.7)

sound["burst"] = love.audio.newSource("sounds/burst.wav")

sound["correct"] = love.audio.newSource("sounds/correct.wav")
-- sound["type"] = love.audio.newSource("sounds/typewriter.wav")
-- sound["type"]:setVolume(1)
function playSound(name)
	love.audio.stop(sound[name])
	love.audio.play(sound[name])
end