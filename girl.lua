girl = class()
girl.images ={
	pants = {
		idle = love.graphics.newImage("graphics/idle-pants.png"),
		happy = love.graphics.newImage("graphics/happy-pants.png"),
		falling = love.graphics.newImage("graphics/falling-pants.png"),
		angry = love.graphics.newImage("graphics/angry-pants.png")
	},
	top = {
		idle = love.graphics.newImage("graphics/idle-top.png"),
		happy = love.graphics.newImage("graphics/happy-top.png"),
		falling = love.graphics.newImage("graphics/falling-top.png"),
		angry = love.graphics.newImage("graphics/angry-top.png")
	},
	mouth = {
		idle = love.graphics.newImage("graphics/idle-mouth.png"),
		happy = love.graphics.newImage("graphics/happy-mouth.png"),
		falling = love.graphics.newImage("graphics/falling-mouth.png"),
		angry = love.graphics.newImage("graphics/angry-mouth.png")
	},
	eyes = {
		idle = love.graphics.newImage("graphics/idle-eyes.png"),
		happy = love.graphics.newImage("graphics/happy-eyes.png"),
		falling = love.graphics.newImage("graphics/falling-eyes.png"),
		angry = love.graphics.newImage("graphics/angry-eyes.png")
	},
	skin = {
		idle = love.graphics.newImage("graphics/idle-skin.png"),
		happy = love.graphics.newImage("graphics/happy-skin.png"),
		falling = love.graphics.newImage("graphics/falling-skin.png"),
		angry = love.graphics.newImage("graphics/angry-skin.png")
		},
	happySprite = love.graphics.newImage("graphics/happy-sprite.png"),
	angrySprite = love.graphics.newImage("graphics/angry-sprite.png"),
	hair = {}
}

for i,v in ipairs(love.filesystem.getDirectoryItems("graphics/hair/")) do
	table.insert(girl.images.hair,{
		idle = love.graphics.newImage("graphics/hair/"..v.."/idle.png"),
		happy = love.graphics.newImage("graphics/hair/"..v.."/happy.png"),
		falling = love.graphics.newImage("graphics/hair/"..v.."/falling.png"),
		angry = love.graphics.newImage("graphics/hair/"..v.."/angry.png"),
		})
end

function girl:init()
	self.x = SPAWN_X
	self.y = SPAWN_Y
	self.fallSpeed = INIT_FALL_SPEED
	self.falling = true
	self.happy = false
	self.angry = false
	self.hasTyping = false
	local g = anim8.newGrid(48,48,192,48)
	self.animation = anim8.newAnimation(g('1-4',1),1)
	self.pantsColor = convertHex(hues.pantsColor[math.random(1,#hues.pantsColor)]:sub(2,7))
	self.topColor = convertHex(hues.topColor[math.random(1,#hues.topColor)]:sub(2,7))
	self.mouthColor = convertHex(hues.mouthColor[math.random(1,#hues.mouthColor)]:sub(2,7))
	self.eyesColor = convertHex(hues.eyesColor[math.random(1,#hues.eyesColor)]:sub(2,7))
	self.skinColor = convertHex(hues.skinColor[math.random(1,#hues.skinColor)]:sub(2,7))
	self.hairColor = convertHex(hues.hairColor[math.random(1,#hues.hairColor)]:sub(2,7))
	self.hairNumber = math.random(1,#girl.images.hair)
	self.image = love.graphics.newImage("assets/head.png")

	self.rotation = 0
end

function girl:update(dt)
	if self.falling then
		self.y = self.y + dt*self.fallSpeed
		if self.y > CONVEYOR_HEIGHT then
			self.falling = false
			local diff = (self.y - CONVEYOR_HEIGHT)/self.fallSpeed
			self.y = CONVEYOR_HEIGHT
			self.x = self.x + diff*conveyorSpeed
			self.box = typebox:new(self)
			self.typing = true
		end
		self.fallSpeed = self.fallSpeed+FALL_GRAVITY*dt
	else
		self.x = self.x + dt*conveyorSpeed
		if self.x > 320-9 then
			self.box.dp2 = vec2(0,0)
			if self.box.active then self.box.active = false; tbh.current = 0 end
		end
	end
	if self.falling then
		self.animation:update(dt*FALLING_ANIMATION_SPEED)
	elseif self.happy then
		self.animation:update(dt*HAPPY_ANIMATION_SPEED)
	elseif self.angry then
		self.animation:update(dt*ANGRY_ANIMATION_SPEED)
	else
		self.animation:update(dt*IDLE_ANIMATION_SPEED)
	end
	if self.typing then self.box.time = (self.x - SPAWN_X)/(320-SPAWN_X) end
	return self.x > 350
end

function girl:draw()
	if self.x > 320-9 then
		self.rotation = (self.x-320+9)/9*math.pi/4
		self.y = CONVEYOR_HEIGHT+(self.x-320+9)/2
	end
	if self.falling then
		love.graphics.setColor(self.skinColor)
		self.animation:draw(girl.images.skin.falling,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.pantsColor)
		self.animation:draw(girl.images.pants.falling,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.topColor)
		self.animation:draw(girl.images.top.falling,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.hairColor)
		self.animation:draw(girl.images.hair[self.hairNumber].falling,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.eyesColor)
		self.animation:draw(girl.images.eyes.falling,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.mouthColor)
		self.animation:draw(girl.images.mouth.falling,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(255,255,255)
	elseif self.happy then
		love.graphics.setColor(self.skinColor)
		self.animation:draw(girl.images.skin.happy,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.pantsColor)
		self.animation:draw(girl.images.pants.happy,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.topColor)
		self.animation:draw(girl.images.top.happy,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.hairColor)
		self.animation:draw(girl.images.hair[self.hairNumber].happy,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.eyesColor)
		self.animation:draw(girl.images.eyes.happy,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.mouthColor)
		self.animation:draw(girl.images.mouth.happy,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(255,255,255)
		self.animation:draw(girl.images.happySprite,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
	elseif self.angry then
		love.graphics.setColor(self.skinColor)
		self.animation:draw(girl.images.skin.angry,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.pantsColor)
		self.animation:draw(girl.images.pants.angry,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.topColor)
		self.animation:draw(girl.images.top.angry,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.hairColor)
		self.animation:draw(girl.images.hair[self.hairNumber].angry,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.eyesColor)
		self.animation:draw(girl.images.eyes.angry,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.mouthColor)
		self.animation:draw(girl.images.mouth.angry,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(255,255,255)
		self.animation:draw(girl.images.angrySprite,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
	else
		love.graphics.setColor(self.skinColor)
		self.animation:draw(girl.images.skin.idle,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.pantsColor)
		self.animation:draw(girl.images.pants.idle,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.topColor)
		self.animation:draw(girl.images.top.idle,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.hairColor)
		self.animation:draw(girl.images.hair[self.hairNumber].idle,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.eyesColor)
		self.animation:draw(girl.images.eyes.idle,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(self.mouthColor)
		self.animation:draw(girl.images.mouth.idle,self.x,self.y,self.rotation,1,1,GIRL_W/2,GIRL_H)
		love.graphics.setColor(255,255,255)
	end
end

