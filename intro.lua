intro = {}

function intro.init() 
	intro.text = {
		"WELCOME TO THE AFFECTION INCOORPORATED EXPERIMENTAL",
		"SPEED-DATE-O-TRON 5000 SPEED-DATING TESTING CHAMBER!",
		"",
		"You are one of the lucky contestants to win a chance",
		"at being part of building the future of the speed-",
		"dating industry!Here at AFFECTION INCOORPORATED, we",
		"take relationships serious,and we have optimized",
		"this new setup for a full utilization of your time!",
		"",
		"All you have to do,to help us enable men and women",
		"of the future to realize their true self through a",
		"meaningful and deep relationship,is to test our new",
		"system by making smalltalk to the girls as they are",
		"presented to you.",
      "",
      "Life could not be easier with",
		"SPEED-DATE-O-TRON 5000!"
	}
	intro.alpha = 255
	intro.timer = 0
end

function intro.update(dt) 
	intro.timer = intro.timer + dt
	if exitm then
		intro.alpha = (1-(intro.timer-intro.mtimer))*255
		if intro.timer - intro.mtimer > 1 then intro.alpha = 0; exit = true end
	end
	if (intro.timer > 10000 or keypressed["return"] or keypressed[" "])and not intro.once then
		intro.once = true
		intro.mtimer = intro.timer
		exitm = true
		modeset = "menu"
	end
end

function intro.draw() 
	love.graphics.setColor(255,255,255,intro.alpha)
   for i,v in ipairs(intro.text) do
      love.graphics.print(v,5,i*10-5)
   end
end

function intro.exit() 
	mode = modeset
end
