tbh = {}
typebox = class()


function typebox:init(girl)
	self.girl = girl
	self.active = false
	local w = typeword:new(words[math.random(minW,maxW)])
	while tbh.startsWith(w:sub(1,1)) do
		w = typeword:new(words[math.random(minW,maxW)])
	end
	self.word = w
	play.increaseWords()
	self.p1 = vec2(SPAWN_X,CONVEYOR_HEIGHT-GIRL_H/2)
	self.p2 = vec2(0,0)
	self.time = 0
	self.dp1 = vec2(75,25)
	self.dp2 = vec2(self.word:len()*6+3,12)
	tbh.add(self)

end

function typebox:update(dt)
	if self.dp1 ~= self.p1 then
		local diff = self.dp1 - self.p1
		local tlen = diff:len()
		if tlen < TEXTBOX_SPEED*dt then
			self.p1 = self.dp1
		else
			diff = diff/tlen
			self.p1 = self.p1 + diff*TEXTBOX_SPEED*dt
		end
	end

	if self.dp2 ~= self.p2 then
		local diff = self.dp2 - self.p2
		local tlen = diff:len()
		if tlen < TEXTBOX_SPEED*dt then
			self.p2 = self.dp2
		else
			diff = diff/tlen
			self.p2 = self.p2 + diff*TEXTBOX_SPEED*dt
		end
	end
end

function typebox:draw()
	if self.p2 ~= vec2(0,0) then
		love.graphics.setColor(255,255-100*self.word.mistakes,255-100*self.word.mistakes)
		love.graphics.rectangle("fill",math.floor(self.p1.x), math.floor(self.p1.y), math.floor(self.p2.x), math.floor(self.p2.y))
		love.graphics.setColor(185-100*self.time,100-50*self.time,100-50*self.time)
		love.graphics.rectangle("fill", math.floor(self.p1.x+1), math.floor(self.p1.y+1), math.floor(self.p2.x-2), math.floor(self.p2.y-2))
		love.graphics.setColor(255,255,255)
		if self.p2 == self.dp2 then self.word:print(self.p1.x+2,self.p1.y+2) end
	end
end

function typebox:typedL(l)
	local check = self.word:typedL(l)
	if check == "angry" then
		self.girl.angry = true
		return true
	elseif check == "happy" then
		playSound("correct")
		wordsCorrect = wordsCorrect + 1
		self.girl.happy = true
		return true
	end
	return false
end

function tbh.init()
	tbh.boxes = {}
	tbh.current = 0
end

function tbh.add(typebox)
	table.insert(tbh.boxes,1,typebox)
	if tbh.current ~= 0 then tbh.current = tbh.current + 1 end
end

function tbh.startsWith(l)
	for i,v in ipairs(tbh.boxes) do
		if l == v.word:sub(1,1) then return true end
	end
	return false
end

function tbh.update(dt)
	for i,v in ipairs(tbh.boxes) do
		v:update(dt)
	end
	local j = 1
	while j <= #tbh.boxes do if tbh.boxes[j].p2 == vec2(0,0) then table.remove(tbh.boxes,j) else j = j+1 end end
	local xpos, ypos = 75,25
	for i,v in ipairs(tbh.boxes) do
		if xpos + v.p2.x > 248 then xpos,ypos = 75,ypos + 14 end
		v.dp1 = vec2(xpos,ypos)
		xpos = xpos + v.p2.x+2
	end
	if tbh.current == 0 and keys[1] then
		for i,v in ipairs(tbh.boxes) do
			if v.word:sub(1,1) == keys[1] then
				tbh.current = i
				v.active = true
				break
			end
		end
	end
	for i,v in ipairs(keys) do
		if tbh.current ~= 0 then
			-- print(tbh.current,tbh.boxes[tbh.current])
			if tbh.boxes[tbh.current] == nil then break end
			local state = tbh.boxes[tbh.current]:typedL(v)
			if state then 
				tbh.boxes[tbh.current].active = false
				tbh.boxes[tbh.current].dp2 = vec2(0,0)
				-- tbh.boxes[tbh.current].dp1 = tbh.boxes[tbh.current].dp1 - vec2(0,100)
				-- table.remove(tbh.boxes,tbh.current)
				tbh.current = 0
			end
		end
	end
end

function tbh.draw(dt)
	for i = #tbh.boxes,1,-1 do--,v in ipairs(tbh.boxes) do
		tbh.boxes[i]:draw()
	end
end