play = {}

dispenseTimes = {Easy = 2.1, Normal = 1.8, Hard = 1.5, Jonathan = 1.2}
conveyorSpeeds = {Easy = 36, Normal = 40, Hard = 44, Jonathan = 48}

function play.init( )
	words,minW,maxW,incMin,incMax = love.filesystem.load("words.lua")()
	girls = {}
	tbh.init()
	girls = {}
	conveyorSpeed = conveyorSpeeds[settings.difficulty]
	dispenseT = dispenseTimes[settings.difficulty]
	table.insert(girls,girl:new())
	dispenseTime = 0
	totaltime = 0
	totalwords = 0
	wordsCorrect = 0
	successes = 0
	love.audio.stop()
	love.audio.play(music["play"])
end

function play.update(dt)
	dispenseTime = dispenseTime + dt
	if dispenseTime > dispenseT and totaltime < 72 then dispenseTime = 0; table.insert(girls,girl:new());dispenser.animation:gotoFrame(1); dispenser.animation:resume();playSound("burst") end

	for i,v in ipairs(girls) do
		local state = v:update(dt)
		if state then table.remove(girls,i) end
	end
	totaltime = totaltime + dt
	if totaltime > 80 then exit=true end
	conveyor.animation:update(dt*conveyorSpeed)
	dispenser.animation:update(dt)
	tbh.update(dt)
	-- for i,v in ipairs(keys) do
	-- 	for k,w in pairs(words) do
	-- 		if k == setword then
	-- 			w:typedL(v)
	-- 		end
	-- 	end
	-- end
end
function play.exit()
	mode = "endscreen"
end

function play.draw()
	love.graphics.draw(mainBackground)
	conveyor.animation:draw(conveyor.image,0,CONVEYOR_HEIGHT-10)
	love.graphics.setBlendMode("additive")
	love.graphics.draw(light)
	love.graphics.setBlendMode("alpha")
	tbh:draw()
	for i,v in ipairs(girls) do
		v:draw()
	end
	player.draw(320/2,170,0,-1,1,50,50)
	dispenser.animation:draw(dispenser.image,SPAWN_X,SPAWN_Y,0,1,1,21,36)
end

function play.increaseWords()
	totalwords = totalwords + 1
	if contains(incMin,totalwords) then minW = minW + 1 end
	if contains(incMax,totalwords) then maxW = maxW + 1 end
end
